# Singleton

## Introducción

El patrón singleton es uno de los más conocidos en ingeniería de software. Básicamente, un singleton es una clase que sólo permite crear una única instancia de sí misma y, por lo general, ofrece un acceso sencillo a esa instancia. Lo más habitual es que los singletons no permitan especificar ningún parámetro al crear la instancia, ya que, de lo contrario, una segunda petición de una instancia con un parámetro diferente podría ser problemática. (Si se debe acceder a la misma instancia para todas las peticiones con el mismo parámetro, el patrón de fábrica es más apropiado). Este artículo trata únicamente de la situación en la que no se requieren parámetros. Normalmente, un requisito de los singletons es que se creen de forma perezosa, es decir, que la instancia no se cree hasta que se necesite por primera vez.

Hay varias formas diferentes de implementar el patrón singleton en C#. Las presentaré aquí en orden inverso de elegancia, empezando por la más común, que no es segura para hilos, hasta llegar a una versión de carga perezosa, segura para hilos, simple y de alto rendimiento.
Sin embargo, todas estas implementaciones comparten cuatro características comunes:

⦁	Un único constructor, privado y sin parámetros. Esto evita que otras clases lo instancien (lo que sería una violación del patrón). Tenga en cuenta que también impide la subclasificación - si un singleton puede ser subclasificado una vez, puede ser subclasificado dos veces, y si cada una de esas subclases puede crear una instancia, se viola el patrón. El patrón de fábrica puede utilizarse si se necesita una única instancia de un tipo base, pero el tipo exacto no se conoce hasta el momento de la ejecución.
⦁	La clase está sellada. Esto es innecesario, estrictamente hablando, debido al punto anterior, pero puede ayudar al JIT a optimizar más las cosas.
⦁	Una variable estática que contiene una referencia a la única instancia creada, si existe.
⦁	Un medio estático público de obtener la referencia a la instancia única creada, creando una si es necesario.
Nótese que todas estas implementaciones también utilizan una propiedad estática pública Instance como medio de acceso a la instancia. En todos los casos, la propiedad podría convertirse fácilmente en un método, sin afectar a la seguridad de los hilos ni al rendimiento.

### Primera versión - no seguro para subprocesos

```c#
// Este cógigo está mal implementado
public sealed class Singleton
{
    private static Singleton instance = null;

    private Singleton()
    {
    }

    public static Singleton Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new Singleton();
            }
            return instance;
        }
    }
}
```

Como se ha indicado antes, el código anterior no es seguro para los hilos. Dos hilos diferentes podrían haber evaluado ambos la prueba if (instance==null) y encontrarla verdadera, entonces ambos crearían instancias, lo que viola el patrón singleton. Tenga en cuenta que, de hecho, la instancia ya puede haber sido creada antes de que se evalúe la expresión, pero el modelo de memoria no garantiza que el nuevo valor de la instancia sea visto por otros hilos a menos que se hayan pasado barreras de memoria adecuadas.

### Segunda versión - seguridad de hilos simple

```c#
public sealed class Singleton
{
    private static Singleton instance = null;
    private static readonly object padlock = new object();

    Singleton()
    {
    }

    public static Singleton Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }
    }
}
```

Esta implementación es segura para los hilos. El hilo toma un bloqueo sobre un objeto compartido, y luego comprueba si la instancia ha sido creada o no antes de crear la instancia. Esto soluciona el problema de la barrera de memoria (ya que el bloqueo asegura que todas las lecturas ocurren lógicamente después de la adquisición del bloqueo, y el desbloqueo asegura que todas las escrituras ocurren lógicamente antes de la liberación del bloqueo) y asegura que sólo un hilo creará una instancia (ya que sólo un hilo puede estar en esa parte del código a la vez - para cuando el segundo hilo entre en él, el primer hilo habrá creado la instancia, por lo que la expresión se evaluará a false). Desafortunadamente, el rendimiento se resiente ya que se adquiere un bloqueo cada vez que se solicita la instancia.

Nótese que en lugar de bloquear en typeof(Singleton) como hacen algunas versiones de esta implementación, bloqueo en el valor de una variable estática que es privada de la clase. Bloquear objetos a los que otras clases pueden acceder y bloquear (como el tipo) puede provocar problemas de rendimiento e incluso bloqueos. Esta es una preferencia general de mi estilo - siempre que sea posible, sólo bloquear objetos específicamente creados con el propósito de bloquear, o que documenten que van a ser bloqueados para propósitos específicos (por ejemplo, para esperar/pulsar una cola). Normalmente, estos objetos deben ser privados para la clase en la que se utilizan. Esto ayuda a que escribir aplicaciones seguras sea mucho más fácil.

### Tercera versión - intento de seguridad de los subprocesos mediante bloqueo de doble comprobación.

```c#
// Este cógigo está mal implementado
public sealed class Singleton
{
    private static Singleton instance = null;
    private static readonly object padlock = new object();

    Singleton()
    {
    }

    public static Singleton Instance
    {
        get
        {
            if (instance == null)
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Singleton();
                    }
                }
            }
            return instance;
        }
    }
}
```

Esta implementación intenta ser segura para los hilos sin la necesidad de sacar un bloqueo cada vez. Desafortunadamente, este patrón tiene cuatro desventajas:

⦁   No funciona en Java. Esto puede parecer algo extraño de comentar, pero merece la pena saberlo si alguna vez necesitas el patrón singleton en Java, y los programadores de C# pueden ser también programadores de Java. El modelo de memoria de Java no garantiza que el constructor se complete antes de que la referencia al nuevo objeto se asigne a la instancia. El modelo de memoria de Java sufrió una remodelación para la versión 1.5, pero el bloqueo de doble comprobación sigue roto después de esto sin una variable volátil (como en C#).

⦁   Sin barreras de memoria, también está roto en la especificación ECMA CLI. Es posible que bajo el modelo de memoria .NET 2.0 (que es más fuerte que la especificación ECMA) sea seguro, pero preferiría no confiar en esa semántica más fuerte, especialmente si hay alguna duda sobre la seguridad. Hacer que la variable de instancia sea volátil puede hacer que funcione, al igual que las llamadas explícitas a barreras de memoria, aunque en este último caso ni siquiera los expertos se ponen de acuerdo sobre qué barreras son necesarias exactamente. Suelo intentar evitar las situaciones en las que los expertos no se ponen de acuerdo sobre lo que está bien y lo que está mal.

⦁	Es fácil equivocarse. El patrón tiene que ser más o menos como el anterior: cualquier cambio significativo puede afectar al rendimiento o a la corrección.

⦁	Sigue sin funcionar tan bien como las implementaciones posteriores.

### Cuarta versión - no tan perezosa, pero segura sin usar bloqueos

```c#
public sealed class Singleton
{
    private static readonly Singleton instance = new Singleton();

    // Constructor estático explícito para indicarle al compilador de C#
    // que no marque el tipo como beforefieldinit
    static Singleton()
    {
    }

    private Singleton()
    {
    }

    public static Singleton Instance
    {
        get
        {
            return instance;
        }
    }
}
```

Como se puede ver, esto es realmente muy simple - pero ¿por qué es thread-safe y qué tan perezoso es? Bien, los constructores estáticos en C# están especificados para ejecutarse sólo cuando una instancia de la clase es creada o un miembro estático es referenciado, y para ejecutarse sólo una vez por AppDomain. Dado que esta comprobación del tipo que se está construyendo necesita ejecutarse pase lo que pase, será más rápido que añadir una comprobación extra como en los ejemplos anteriores. Sin embargo, hay un par de problemas:

⦁	No es tan perezosa como las otras implementaciones. En particular, si tienes miembros estáticos distintos de Instance, la primera referencia a esos miembros implicará la creación de la instancia. Esto se corrige en la siguiente implementación.

⦁	Hay complicaciones si un constructor estático invoca a otro que vuelve a invocar al primero. Busca en las especificaciones de .NET (actualmente en la sección 9.5.3 de la partición II) más detalles sobre la naturaleza exacta de los inicializadores de tipo - es poco probable que te muerdan, pero merece la pena ser consciente de las consecuencias de los constructores estáticos que se refieren unos a otros en un ciclo.

⦁	La pereza de los inicializadores de tipo sólo está garantizada por .NET cuando el tipo no está marcado con una bandera especial llamada beforefieldinit. Desafortunadamente, el compilador de C# (tal y como se proporciona en el runtime de .NET 1.1, al menos) marca todos los tipos que no tienen un constructor estático (es decir, un bloque que parece un constructor pero está marcado como estático) como beforefieldinit.

Un atajo que se puedes tomar con esta implementación (y sólo con ésta) es hacer de instance una variable pública estática de sólo lectura, y deshacerse de la propiedad por completo. Esto hace que el esqueleto de código básico sea absolutamente diminuto. Mucha gente, sin embargo, prefiere tener una propiedad en caso de que se necesite una acción adicional en el futuro, y es probable que JIT inlining haga que el rendimiento sea idéntico. (Tenga en cuenta que el propio constructor estático sigue siendo necesario si necesita pereza).

### Quinta versión - instanciación totalmente perezosa

```c#
public sealed class Singleton
{
    private Singleton()
    {
    }

    public static Singleton Instance { get { return Nested.instance; } }

    private class Nested
    {
        // Constructor estático explícito para indicarle al compilador de C#
        // que no marque el tipo como beforefieldinit
        static Nested()
        {
        }

        internal static readonly Singleton instance = new Singleton();
    }
}
```

En este caso, la instanciación se activa con la primera referencia al miembro estático de la clase anidada, que sólo se produce en Instance. Esto significa que la implementación es totalmente perezosa, pero tiene todas las ventajas de rendimiento de las anteriores. Nótese que aunque las clases anidadas tienen acceso a los miembros privados de la clase que las encierra, no ocurre lo mismo a la inversa, de ahí la necesidad de que Instance sea interna aquí. Sin embargo, esto no plantea ningún otro problema, ya que la propia clase es privada. Sin embargo, el código es un poco más complicado para que la instanciación sea perezosa.

### Sexta versión - uso del tipo Lazy<T> de .NET 4

Si estás usando .NET 4 (o superior), puedes usar el tipo System.Lazy<T> para hacer la pereza realmente simple. Todo lo que necesitas hacer es pasar un delegado al constructor que llama al constructor Singleton - lo que se hace más fácilmente con una expresión lambda.

```c#
public sealed class Singleton
{
    private static readonly Lazy<Singleton> lazy =
        new Lazy<Singleton>(() => new Singleton());

    public static Singleton Instance { get { return lazy.Value; } }

    private Singleton()
    {
    }
}
```

Es simple y funciona bien. También te permite comprobar si la instancia ha sido creada o no con la propiedad IsValueCreated, si lo necesitas.
El código anterior utiliza implícitamente LazyThreadSafetyMode.ExecutionAndPublication como modo de seguridad de hilos para el Lazy<Singleton>. Dependiendo de sus necesidades, es posible que desee experimentar con otros modos.

### Rendimiento frente a pereza

En muchos casos, en realidad no necesitarás pereza total - a menos que la inicialización de tu clase haga algo que consuma mucho tiempo, o tenga algún efecto secundario en otra parte, probablemente esté bien omitir el constructor estático explícito mostrado arriba. Esto puede aumentar el rendimiento, ya que permite al compilador JIT hacer una única comprobación (por ejemplo, al comienzo de un método) para asegurarse de que el tipo ha sido inicializado, y luego asumirlo a partir de entonces. Si su instancia singleton es referenciada dentro de un bucle relativamente estrecho, esto puede suponer una diferencia de rendimiento (relativamente) significativa. Deberías decidir si la instanciación totalmente perezosa es necesaria o no, y documentar esta decisión apropiadamente dentro de la clase.

Gran parte de la razón de la existencia de esta página es que la gente trata de ser inteligente, y por lo tanto se le ocurrió el algoritmo de bloqueo de doble comprobación. Hay una actitud de que el bloqueo es caro que es común y equivocada. He escrito un benchmark muy rápido que simplemente adquiere instancias singleton en un bucle de mil millones de maneras, probando diferentes variantes. No es terriblemente científico, porque en la vida real es posible que quieras saber lo rápido que es si cada iteración implica realmente una llamada a un método que obtiene el singleton, etc. Sin embargo, muestra un punto importante. En mi portátil, la solución más lenta (por un factor de aproximadamente 5) es la de bloqueo (solución 2). ¿Es importante? Probablemente no, si tenemos en cuenta que aún así consiguió adquirir el singleton mil millones de veces en menos de 40 segundos. (Nota: este artículo se escribió originalmente hace ya bastante tiempo - yo esperaría un mejor rendimiento ahora). Esto significa que si "sólo" estás adquiriendo el singleton cuatrocientas mil veces por segundo, el coste de la adquisición va a ser el 1% del rendimiento - por lo que mejorarlo no va a hacer mucho. Ahora bien, si estás adquiriendo el singleton tan a menudo, ¿no es probable que lo estés utilizando dentro de un bucle? Si tanto te importa mejorar un poco el rendimiento, ¿por qué no declarar una variable local fuera del bucle, adquirir el singleton una vez y luego hacer el bucle? Bingo, incluso la implementación más lenta se vuelve fácilmente adecuada.

### Excepciones

A veces, necesitas hacer trabajo en un constructor singleton que puede lanzar una excepción, pero puede que no sea fatal para toda la aplicación. 

Potencialmente, tu aplicación puede ser capaz de arreglar el problema y querer intentarlo de nuevo. El uso de inicializadores de tipo para construir el singleton se vuelve problemático en esta etapa. Diferentes tiempos de ejecución manejan este caso de forma diferente, pero no conozco ninguno que haga lo deseado (ejecutar el inicializador de tipo de nuevo), e incluso si alguno lo hiciera, tu código estaría roto en otros tiempos de ejecución. Para evitar estos problemas, yo sugeriría utilizar el segundo patrón que aparece en la página - sólo tiene que utilizar un simple bloqueo, y pasar por la comprobación cada vez, la construcción de la instancia en el método / propiedad si no ha sido ya construido con éxito.

